<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;

    protected $guarded = [];

    /**
     * Returns the url path for a company
     * @return  string
     */
    public function path()
    {
        return "/companies/{$this->id}";
    }

    /**
     * returns all employees for a company
     * @return  
     */
    public function employees()
    {
        return $this->hasMany(Employee::class, 'company_id', 'id');
    }
}

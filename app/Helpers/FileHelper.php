<?php

namespace App\Helpers;
use Illuminate\Support\Facades\Storage;

class FileHelper
{
	/**
     * Save an image to the disk
     * @param  string $url  
     * @param  string $path 
     * @param  string $disk 
     * @return string     
     */
    public static function saveImage($url, $path, $disk)
    {
        $fileContent = @file_get_contents($url,
            false,
            stream_context_create([
                    'ssl' => [
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                    ],
                ]
            ));
        Storage::disk($disk)->put($path, $fileContent);

        return $path;
    }

}
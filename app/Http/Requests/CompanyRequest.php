<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Intervention\Image\ImageManagerStatic as Image;

class CompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'    => 'required',
            'email'   => ['required', 'unique:companies'],
            'logo'    => ['url', function($attribute, $value, $fail) {
                $img = Image::make($value);

                $minWidth = 100;
                $minHeight = 50;

                if($img->width() < $minWidth || $img->height() < $minHeight) {
                    $fail('The '.$attribute.' is the wrong size.');
                }
            }],
            'website' => 'url'
        ];
    }
}

<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\EmployeeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

Route::get('/companies', [CompanyController::class, 'index'])
                ->middleware('auth');
Route::get('/companies/create', [CompanyController::class, 'create'])
                ->middleware('auth');
Route::get('/companies/{company}/edit', [CompanyController::class, 'edit'])
                ->middleware('auth');

Route::get('/companies/{company}', [CompanyController::class, 'show'])
                ->middleware('auth');
Route::post('/companies', [CompanyController::class, 'store'])
                ->middleware('auth');
Route::patch('/companies/{company}', [CompanyController::class, 'update'])
                ->middleware('auth');


Route::post('/employees', [EmployeeController::class, 'store'])
                ->middleware('auth');
Thank you for taking the time to consider me for your role.

This following application has been written in a TDD way.

It demonstrates the following:

- Tests feature and unit
- DB migrations
- DB relations
- DB seed
- Factories
- Rules
- Resource controllers
- Pagination
- Request classes
- Third party library usage
- Use of blade
- Use of Laravel auth
- CRUD - missing some of this due to time 

Had i had more time to spend on this i would have finished the following

- More test coverage (currently only done a few on each type of function)
- Fixed image testing
- Completed the UI
- Completed the CRUD
- Completed Employees controller
- Made pagination work through Ajax
- Refactored some code duplication i.e creating a user and signing in for tests


I believe the code demonstrates broad knowledge of Laravel and its development principles. I have not used Laravel 8 or the admin theme so shows I am able to pick up new tools independently.

    @csrf
    <div class="row">
        <x-adminlte-input name="name" label="Name" placeholder="ABC Company" value="{{$company->name}}"
            fgroup-class="col-md-6" disable-feedback/>
    </div>
    <div class="row">
        <x-adminlte-input name="email" label="Email" placeholder="company@email.com" value="{{$company->email}}"
            fgroup-class="col-md-6" disable-feedback/>
    </div>
    <div class="row">
        <x-adminlte-input name="logo" label="Logo" placeholder="your logo url" value="{{$company->logo}}"
            fgroup-class="col-md-6" disable-feedback/>
    </div>
    <div class="row">
        <x-adminlte-input name="website" label="Website" placeholder="https://example.com" value="{{$company->website}}"
            fgroup-class="col-md-6" disable-feedback/>
    </div>
    <x-adminlte-button type="submit" label="{{$buttonText}}"/>


    @if($errors->any())
        <div class="mt-6 text-small text-red">
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </div>
    @endif
@extends('adminlte::page')

@section('title', 'Companies - Edit')

@section('content_header')
    <h1>Edit {{$company->name}}</h1>
@stop

@section('content')
    <form action="{{$company->path()}}" method="POST">
        @method('PATCH') 
        
        @include('company.form', [
            'buttonText' => 'Update',
        ])
    </form>
    
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')

@stop
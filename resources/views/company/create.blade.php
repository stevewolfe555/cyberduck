@extends('adminlte::page')

@section('title', 'Companies - Edit')

@section('content_header')
    <h1>Crewate Company</h1>
@stop

@section('content')
    <form action="/companies" method="POST">

        @include('company.form', [
            'buttonText' => 'Create'
        ])

    </form>

    @if($errors->any())
        <div class="mt-6 text-small text-red">
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </div>
    @endif
    
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')

@stop
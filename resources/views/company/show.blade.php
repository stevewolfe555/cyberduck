@extends('adminlte::page')

@section('title', 'Company')

@section('content_header')
    <h1>{{$company->name}}</h1>
@stop

@section('content')

    <div class="row">
        {{$company->name}}
    </div>
    <div class="row">
        {{$company->email}}
    </div>
    <div class="row">
        <img src="{{asset('storage/'.$company->logo)}}">
    </div>
    <div class="row">
        <a href="{{$company->website}}">{{$company->website}}</a>
    </div>
    
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')

@stop
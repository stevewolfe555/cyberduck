Complete a small test in Laravel – to develop a basic mini CRM to manage


companies and their employees
- Use https://adminlte.io/ as a framework for the application

- Basic Laravel Auth: ability to log in as administrator - done

- Use database seeds to create first user with email admin@admin.com and password
“password” - done

- CRUD functionality (Create / Read / Update / Delete) for two menu items: Companies and
Employees.

- Companies DB table consists of these fields: Name (required), email, logo - done

 (minimum -%%×-%%), website - done

- Employees DB table consists of these fields: First name (required), last name (required),
Company (foreign key to Companies), email, phone - done

- Use database migrations to create those schemas above - done

- Store companies’ logos in storage/app/public folder and make them accessible from public - done

- Use basic Laravel resource controllers with default methods – index, create, store etc. - done

- Use Laravel’s validation function, using Request classes - done 

- Use Laravel’s pagination for showing Companies/Employees list, -% entries per page - done

- Use Laravel make:auth as default Bootstrap-based design theme, - using laravel 8s new Breeze (not used eaither of these before) -done
 but remove ability to register - done


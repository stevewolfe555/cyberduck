<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\Employee;
use App\Models\Company;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeTest extends TestCase
{
    use RefreshDatabase;

    /** @test*/
    public function belongs_to_a_company()
    {
        $employee = Employee::factory()->create();
        $this->assertInstanceOf(Company::class, $employee->company);

    }
}

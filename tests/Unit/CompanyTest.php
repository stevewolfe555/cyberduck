<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\Company;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Database\Eloquent\Collection;

class CompanyTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_has_a_path() 
    {

        $company = Company::factory()->create();

        $this->assertEquals('/companies/' . $company->id, $company->path());
    }

    /** @test*/
    public function has_employees()
    {
        $company = Company::factory()->create();

        $this->assertInstanceOf(Collection::class, $company->employees);

    }
}

<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Models\User;
use App\Models\Employee;

class EmployeeTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function an_employee_must_have_a_required_fields()
    {

        $user = User::factory()->create(['email' => 'admin@admin.com']);
        $this->actingAs($user);


        $attributes = Employee::factory()->raw([
            'firstname' => '',
            'lastname' => '',
        ]);

        $this->post('/employees', $attributes)
            ->assertSessionHasErrors('firstname')
            ->assertSessionHasErrors('lastname');


    }


    /** @test */
    public function an_employee_must_belong_to_a_company()
    {
        $this->markTestSkipped('must be revisited.');
    }
    
}

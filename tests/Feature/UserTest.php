<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

use App\Models\User;
use Illuminate\Support\Facades\Auth;


class UserTest extends TestCase
{
    use RefreshDatabase;
 
    /** @test */
    public function a_user_can_log_in_as_admin() 
    {   
        $user = User::factory()->create(['email' => 'admin@admin.com']);
        $this->actingAs($user);
        
        $this->assertTrue(Auth::check());
    }

    /** @test */
    public function a_visitor_should_not_be_able_to_register()
    {
        $this->get('/register')->assertStatus(404);

        $this->post('/register', [
            'name' => 'Some User',
            'email' => 'some@example.com',
            'password' => 'password',
            'confirmed' => 'password'
        ])->assertStatus(404);
    }
}

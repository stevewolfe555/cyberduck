<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Models\User;
use App\Models\Company;

class CompanyTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_user_can_create_a_company()
    {
        $this->withoutExceptionHandling();

        $user = User::factory()->create(['email' => 'admin@admin.com']);
        $this->actingAs($user);

        $attributes = [
            'name'    => 'A company',
            'email'   => 'example@example.com',
            // 'logo'    => 'https://placeimg.com/640/480/any',
            'website' => 'https://example.com'
        ];

        // $this->post('/companies', $attributes)->assertStatus('302');


        $response = $this->post('/companies', $attributes);

        $company = Company::where($attributes)->get()->toArray();

        $this->assertCount(1, $company);

        // $response->assertRedirect($company->path());

        // $this->get($company->path())
        //     ->assertSee($attributes['name'])
        //     ->assertSee($attributes['email'])
        //     ->assertSee($attributes['logo']);
    }

    /** @test */
    public function a_user_can_edit_a_company()
    {
        $this->withoutExceptionHandling();
        
        $user = User::factory()->create(['email' => 'admin@admin.com']);
        $this->actingAs($user);


        $company = Company::factory()->create();

        $this->patch('/companies/' .$company->id, [
            'name'    => 'Changed',
            'email'   => 'Changed@example.com',
            // 'logo'    => 'https://Changed.com/640/480/any',
            'website' => 'https://Changed.com'
        ]);

        $this->assertDatabaseHas('companies', [
            'name'    => 'Changed',
            'email'   => 'Changed@example.com',
            // 'logo'    => 'https://Changed.com/640/480/any',
            'website' => 'https://Changed.com'
        ]);

    }
    
}
